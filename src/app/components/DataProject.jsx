const data =[
  {
    id:1,
    title:"Bnb_Project",
    mieuta:"Website đặt phòng",
    image:"/img/bnb.jpg",
    tech:"Reactjs/Redux/Axios/Antd/TaiwindCSS/Bootstrap...",
    quantity:"5 Người",
    linkDemo:"https://samar-rho-eight.vercel.app/",
    link:"https://gitlab.com/cuongtc1003/bnb_project",
    position:"Xây Dựng Trang Admin"
   },
  { 
    id:2,
    title:"Movie_Project",
    mieuta:"Website Rạp Chiếu Phim",
    tech:"Reactjs/Antd/TaiwindCSS/Axios/Redux,..",
    image:"/img/movie_project.jpg",
    quantity:"3 Người",
    linkDemo:"https://samar-rho-eight.vercel.app/",
    link:"https://gitlab.com/cuongtc1003/bc41_capstone_movie",
    position:"Xây Dựng Layout"
  },
  {
    id:3,
    title:"Sama",
    mieuta:"Website Blog",
    tech:"Html/Css/Animation/Jquery,..",
    image:"/img/samar.jpg",
    quantity:"1 Người",
    linkDemo:"https://samar-rho-eight.vercel.app/",
    link:"https://gitlab.com/cuongtc1003/samar ",
    position:"Xây Dựng Layout"
   },
]
export default data;