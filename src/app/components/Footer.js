import React from "react";
import Image from "next/image";
import { FaFacebookSquare,FaInstagramSquare,FaGithub } from "react-icons/fa";
function Footer() {
  return (
    <div style={{ height: "150vh" }} id="footer" className=" bg-[#3457a2] mt-10">
     <div className="list-footer sm:grid sm:grid-cols-1 sm:text-center md:grid-cols-2  lg:grid-cols-4">
      <div className="first-footer">
      <div className="img-footer sm:ml-60 md:ml-20 lg:ml-0">
        <Image
          src={"/img/snapedit_1694960596818.png"}
          width={200}
          height={150}
        />
      </div>
        Lorem ipsum, dolor sit amet <br /> consectetur adipisicing elit. <br />  Modi rem magni
        reprehenderit <br /> possimus atque nisi deserunt <br /> et accusamus alias
        consequatur!
      </div>
      <div className="second-footer md:pt-10 lg:pt-40 text-left pl-10">
        <h2 className="font-medium">Product</h2>
        <ul className="space-y-2 text-slate-300">
          <li>Theme Design</li>
          <li>Plugin Design</li>
          <li>Wordpress</li>
          <li>Joomla Template</li>
          <li>HTML Template</li>
        </ul>
      </div>
      <div className="second-footer md:pt-10 lg:pt-40 text-left">
        <h2 className="font-medium">Useful Link</h2>
        <ul className="space-y-2 text-slate-300">
          <li>Blog</li>
          <li>Pricing</li>
          <li>Sales</li>
          <li>Tickets</li>
          <li>Customer Services</li>
        </ul>
      </div>
      <div className="second-footer md:pt-10 lg:pt-40 text-left">
        <h2 className="font-medium">Address</h2>
        <ul className="space-y-2 text-slate-300">
          <li>127,Westwood Lane</li>
          <li>DA15 9FS,Sidup</li>
          <li>London,UK</li>
        </ul>
      </div>
     </div>
     <div className="contact flex justify-center space-x-3 text-3xl mt-20">
        <div><a  className="bg-blue-200 border-black" href="https://www.facebook.com/profile.php?id=100024636332556"><FaFacebookSquare/></a></div>
        <div className="bg-purple-400 rounded-md"><a href="https://www.instagram.com/__nc03_/"><FaInstagramSquare/></a></div>
        <div className="bg-slate-500 rounded-full"><a className="border-slate-300" href="https://github.com/NgCuong-Dev"><FaGithub/></a></div>
     </div>
    </div>
  );
}

export default Footer;
